# Rust Mosaik API

This is a rust implementation of the [mosaik client API](https://mosaik.readthedocs.io/en/latest/mosaik-api/low-level.html). It is intended to be a library to use mosaik in rust.

## Development

This repository is infrequently in development.
Currently, basic simulator is working

## Current development goals

To reach 1.0, the following points must still be implemented.

- Release lib to cargo

## License 

This project is under the MIT License, see file for details.