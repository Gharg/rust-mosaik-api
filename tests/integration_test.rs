use anyhow::{anyhow, Result};
use mosaik_rust_api::create_model::CreateModel;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value};
use std::collections::HashMap;
use std::str::FromStr;
use std::process::Command;
use mosaik_rust_api::simulator::{Simulator, Entity};
use mosaik_rust_api::server::SimServer;
use mosaik_rust_api::*;

#[derive(Deserialize, Serialize, Debug)]
struct TestEntity {
    id: String,
    val: i64,
    delta: i64,
    requested: bool
}

impl Entity for TestEntity {
    fn get_children<'a>(&self) -> Vec<&'a dyn Entity> {
        let e: Vec<&'a dyn Entity> = vec![];
        e
    }
    fn get_entity_id(&self) -> String {
        String::from_str("foo").unwrap()
    }
    fn get_rel(&self) -> Vec<String> {
        vec![String::from_str("foo").unwrap()]
    }
    fn get_type(&self) -> String {
        String::new()
    }

    fn to_model(&self) -> CreateModel {
        CreateModel {
            eid: &self.id,
            model_type: "ExampleModel",
            children: vec![],
            rel: vec![]
        }
    }
}

struct TestSim {
    prefix: String,
    step_size: i64,
    entities: HashMap<String, TestEntity>,
}

impl TestSim {
    fn get_metadata(&self) -> metadata::Metadata {
        metadata::Metadata {
            api_version: API_VERSION,
            simulator_type: metadata::SimulatorType::TimeBased,
            models: HashMap::from([(
                "ExampleModel",
                metadata::Model {
                    public: true,
                    params: &["init_val"],
                    attrs: &["val", "delta", "requested"],
                    any_inputs: false,
                },
                
            )]),
            extra_methods: &[],
        }
    }
}

impl<'a> Simulator<'a> for TestSim {
    fn initialize<'b>(
        &'b mut self,
        sim_id: String,
        _time_resolution: f64,
        sim_params: &HashMap<String, Value>,
    ) -> Result<metadata::Metadata> {
        println!("Simid: {}", sim_id);
        for m in sim_params {
            println!("({}, {})", m.0, m.1);
        }
        self.step_size = sim_params.get("step_size").map_or_else(|| Ok(60), |v: &Value| v.as_i64().ok_or( anyhow!("step_size has the wrong type")))?;
        self.prefix = sim_params.get("eid_prefix").map_or_else(|| Ok(""), |v: &Value| v.as_str().ok_or( anyhow!("step_size has the wrong type")))?.to_string();
        Ok(self.get_metadata())
    }
    fn create<'b: 'c, 'c>(&'b mut self, model_amount: u64, model_name: &str, kwargs: &HashMap<String, Value>) -> Result<Vec<&'c dyn Entity>> {
        assert!(model_name == "ExampleModel", "Model name is {:?} not 'ExampleModel'!", model_name);
        let mut entity_ids: Vec<String> = vec![];
        for i in self.entities.len()..self.entities.len()+model_amount as usize {
            let init_val = kwargs.get("init_val").map_or(Ok(0), |v| v.as_i64().ok_or( anyhow!("init_val has the wrong type")))?;
            let id = self.prefix.clone() + &i.to_string();
            entity_ids.push(id.clone());
            self.entities.insert(id.clone(), TestEntity { id: id, val: init_val, delta: 1, requested: false});
        }        

        let mut v: Vec<&'c dyn Entity> = vec![];
        for e in entity_ids.iter() {
            let entity = self.entities.get(e).ok_or(anyhow::anyhow!("Entity {:?} not found", e))?;
            v.insert(v.len(), entity as &dyn Entity)
        }
        //self.entities.extend(method_entities);
        Ok(v)
    }

    fn get_data(&self, output: &Map<String, Value>) -> Result<HashMap<String, HashMap<String, Value>>> {
        let mut map = HashMap::new();
        for (eid, attrs) in output {
            print!("eid {}", eid);
            map.insert(eid.clone(), HashMap::new());
            let attributes = attrs.as_array().ok_or(anyhow::anyhow!("Output malformed {:?}", attrs.to_string()))?;
            for attr in  attributes {                
                let entity = self.entities.get(eid).ok_or(anyhow::anyhow!("Eid not found under entities {:?}", eid))?;
                let attribute = attr.as_str().ok_or(anyhow!("Attribute is not a string"))?.to_string();
                let value_entity = serde_json::to_value(&entity)?;
                
                map.get_mut(eid).ok_or(anyhow::anyhow!("eid not found"))?
                .insert(attribute.clone(), value_entity.get(attribute).ok_or(anyhow!("Attribute not found"))?.clone());
            }
        }
        Ok(map)
    }

    fn setup_done(&self) -> Result<()> {
        Ok(())
    }

    fn step(& mut self, time: f64, input: &Map<String, Value>) -> Result<f64> { //, _max_advance: f64
        for (eid, attrs) in input {
            match attrs.as_object().ok_or(anyhow!("Input malformed! {:?}", attrs))?.get("delta") {
                Some(sid_map) => {
                    let altered_delta = sid_map.as_object().ok_or(anyhow!("Attribute-Values map is malformed! {:?}", attrs))?
                    .values()
                    .map(|v| v.as_i64().ok_or(anyhow!("Value is not i64, {:?}", v)))
                    .collect::<Result<Vec<_>>>()?.iter().sum();
                    self.entities.get_mut(eid).ok_or(anyhow!("Eid not found"))?.delta = altered_delta;
                },
                None => ()
            }
        }

        for entity in self.entities.values_mut() {
            entity.val += entity.delta;
        }
            
        Ok(time + 1f64)
    }

    fn stop<'b>(&mut self) -> Result<()> {
        self.entities.clear();
        Ok(())
    }
}

#[test]
fn test_service<'a>() {
    let simulator = TestSim{prefix: "".to_string(), step_size: 1i64, entities: HashMap::new()};
    let simulator_box: Box<dyn Simulator> = Box::new(simulator);
    let mut sim_server: SimServer<'a> = SimServer {
        simulator: simulator_box
    };
    let mut output = Command::new("python.exe").current_dir("tests").arg("demo1.py")
    .spawn()
    .unwrap();

    sim_server.start(Option::Some("0.0.0.0"), Option::Some(5878)).unwrap();
    assert!(output.wait().unwrap().success());
}
