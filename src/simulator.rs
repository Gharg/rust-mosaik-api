//! This module contains the trait for Simulator and Entity.
//! 
//! The entity trait is needed for the api to ensure the right format for some messages, i.e. the CreateModel when a model is created.

use anyhow::Result;
use serde_json::{Map, Value};
use std::collections::HashMap;
use crate::metadata::Metadata;
use crate::create_model::CreateModel;

/// An entity for mosaik should implement this trait. The Server uses these functions to parse the data into mosaik messages
pub trait Entity {
    /// Get the id of an entity. May be replaced later
    fn get_entity_id(&self) -> String;
    /// Gets type of the model. Make sure to let the return match with the MetaModel model name
    fn get_type(&self) -> String;
    /// List of the related entities (Currently not used by this library)
    fn get_rel(&self) -> Vec<String>;
    /// Get childrens of this entity. (Currently not used by this library)
    fn get_children<'a>(&self) -> Vec<&'a dyn Entity>;
    /// Get the related CreateModel (used to send created model info to mosaik)
    fn to_model(&self) -> CreateModel;
}

/// Implements the Simulator trait that is the interface between rust and the mosaik api
pub trait Simulator<'a> {
    /// Initialize function of mosaik
    fn initialize<'b>(
        &'b mut self,
        sim_id: String,
        time_resolution: f64,
        sim_params: &HashMap<String, Value>,
    ) -> Result<Metadata>;
    
    /// Create function of mosaik. Returns the list of created entities.
    fn create<'b: 'c, 'c>(&'b mut self, model_amount: u64, model_name: &str, kwargs: &HashMap<String, Value>) -> Result<Vec<&'c dyn Entity>>;
    
    /// Will be called when setup is done, just before simulation begins. Use it to clean up the setup and prepare the simulation.
    fn setup_done<'b>(&'b self) -> Result<()>;
    
    /// Step of the simulation. Returns the time when the next step of the simulation will be called.
    fn step<'b>(&'b mut self, time: f64, input: &Map<String, Value>) -> Result<f64>; //, max_advance: f64
    
    /// Get data step of mosaik. Output contains Map<String, &Vec<String>>. Returns as result a HashMap<String, HashMap<String, Value>>> which should contains:
    /// {
    ///   "eid_1: {
    ///     "attr_1": "val_1",
    ///     "attr_2": "val_2",
    ///     ...
    ///   },
    /// }
    /// 
    fn get_data(&self, output: &Map<String, Value>) -> Result<HashMap<String, HashMap<String, Value>>>;
    
    /// Stop signal from mosaik. Can be used for a cleanup after the simulation has ended.
    fn stop<'b>(&mut self) -> Result<()>;
}