//! This library is to implement a mosaik simulator For more information, see <https://gitlab.com/Gharg/rust-mosaik-api>.
//! 
//! For an simulator to work, an entity and simulator must be implemented. When these are implemented, a server must be started.
//! Minimal Example:
//! ```
//! use mosaik_rust_api::simulator::{Simulator, Entity};
//! use mosaik_rust_api::server::SimServer;
//! 
//! fn start_service(simulator_box: Box<dyn Simulator>) {
//!   // Create simulation server that mosaik can connect to
//!   let mut sim_server: SimServer = SimServer { simulator: simulator_box };
//!   sim_server.start(
//!   Option::Some("0.0.0.0"), // Define enpoint for mosaik
//!   Option::Some(5878) // Define port for mosaik
//!   ).unwrap();
//! }
//! ```

pub mod metadata;
pub mod message;
pub mod server;
pub mod simulator;
pub mod create_model;

/// Current API version that mosaik api rust was developed for
pub const API_VERSION: &str = "3.0";