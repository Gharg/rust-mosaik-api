use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct CreateModel<'a> {
    pub eid: &'a str,
    #[serde(rename = "type")]
    pub model_type: &'a str,
    pub rel: Vec<&'a str>,
    pub children: Vec<CreateModel<'a>>
}