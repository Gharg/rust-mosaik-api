pub mod mosaik_method;
pub mod payload;
pub mod message_with_type;
pub mod request;
pub mod response;

use payload::Payload;
use anyhow::{Result, anyhow};
use std::str::FromStr;
use serde::Serialize;

use message_with_type::MessageWithType;
use response::Response;

#[derive(Debug, Serialize)]
pub struct Message {
    pub header: usize,
    pub payload: Payload,
}

impl Message {
    pub fn try_parse_request(s: String, n: usize) -> Result<Message> {
        println!("To parse: {}", s);
        Ok(Message {
            header: n,
            payload: s.parse()?,
        })
    }
}

impl TryFrom<Payload> for Message {
    type Error = anyhow::Error;

    fn try_from(payload: Payload) -> Result<Self, Self::Error> {
        match payload.content {
            MessageWithType::Request(_r) => Err(anyhow!("Cannot parse requests!")),
            MessageWithType::Response(r) => {
                let u = r.content.capacity();
                Ok(Message {
                    header: u,
                    payload: Payload {
                        message_type: payload.message_type,
                        id: payload.id,
                        content: MessageWithType::Response(Response {
                            content: String::from_str(&r.content)?,
                        }),
                    },
                })
            }
        }
    }
}