use std::str::FromStr;
use anyhow::{Result, anyhow};
use serde::Serialize;
use serde_json::Value;
use crate::message::message_with_type::MessageWithType;

#[derive(Debug, Serialize)]
pub struct Payload {
    pub message_type: usize,
    pub id: usize,
    pub content: MessageWithType, //Request
}

impl FromStr for Payload {
    type Err = anyhow::Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        // TODO: less cloning
        let array = serde_json::from_str::<Value>(value)?;
        let array = array.as_array().ok_or(anyhow!("Could not parse payload as array"))?;
        let message_type: usize = serde_json::from_value(array[0].clone())?;
        let id = serde_json::from_value(array[1].clone())?;
        let content = MessageWithType::Request(serde_json::from_value(array[2].clone())?);
        Ok(Payload {
            message_type,
            id,
            content,
        })
    }
}