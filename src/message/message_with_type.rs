use serde::Serialize;
use crate::message::request::Request;
use crate::message::response::Response;

#[derive(Debug, Serialize)]
pub enum MessageWithType {
    Request(Request),
    Response(Response),
}