use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
pub enum MosaikMethod {
    #[serde(rename = "init")]
    Init,
    #[serde(rename = "create")]
    Create,
    #[serde(rename = "setup_done")]
    SetupDone,
    #[serde(rename = "step")]
    Step,
    #[serde(rename = "get_data")]
    GetData,
    #[serde(rename = "stop")]
    Stop,
    //async_get_progress, // Not implemented
    //async_get_related_entities,
    //async_get_data,
    //async_set_data,
    //async_set_event,
}