use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use crate::message::mosaik_method::MosaikMethod;

#[derive(Deserialize, Serialize, Debug)]
pub struct Request {
    pub function: MosaikMethod,
    pub args: Vec<Value>,
    pub kwargs: HashMap<String, Value>,
}

impl Request {
    pub fn new(_s: &mut String) -> Request {
        Request {
            function: MosaikMethod::Init,
            args: vec![],
            kwargs: HashMap::new(),
        }
    }
}