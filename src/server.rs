//! This implements the server for a mosaik simulation.
//! 
//! The SimServer is the glue between your mosaik rust simulator and the python mosaik server.
//! You need to pass s simulator that will be executed
//! 
//! To start the simulation, first initialize your simulator, then initialize the SimServer and pass the simulator, then start it:
//! ```
//! use mosaik_rust_api::simulator::{Simulator, Entity};
//! use mosaik_rust_api::server::SimServer;
//! 
//! fn start_service(simulator_box: Box<dyn Simulator>) {
//!   // Create simulation server that mosaik can connect to
//!   let mut sim_server: SimServer = SimServer { simulator: simulator_box };
//!   sim_server.start(
//!   Option::Some("0.0.0.0"), // Define enpoint for mosaik
//!   Option::Some(5878) // Define port for mosaik
//!   ).unwrap();
//! }
//! ```

use anyhow::{anyhow, Result};
use std::net::{TcpListener, TcpStream};
use std::io::{Read, Write};
use crate::simulator::Simulator;
use crate::message::Message;
use crate::message::request::Request;
use crate::message::response::Response;
use crate::message::mosaik_method::MosaikMethod;
use crate::message::message_with_type::MessageWithType;

/// SimServer struct, see server module for more information.
pub struct SimServer<'a> {
    pub simulator: Box<dyn Simulator<'a>>,
}

impl<'a> SimServer<'a> {
    /// Pass binding address and port to the simulator. These parameters are optional and standard is
    /// ```
    /// use anyhow::Result;
    /// use mosaik_rust_api::server::SimServer;
    /// fn start(mut sim_server: SimServer) -> Result<()> {
    ///   sim_server.start(Option::Some("127.0.0.1"), Option::Some(7878))
    /// }
    /// ```
    pub fn start<'c>(&'c mut self, _binding: Option<&str>, _port: Option<i64>) -> Result<()> {
        let binding = match _binding {
            Some(e) => e,
            None => "127.0.0.1"
        };
        
        let port = match _port {
            Some(e) => e,
            None => 7878
        };

        let listener = TcpListener::bind(binding.to_owned() + ":" + &port.to_string()).unwrap();

        for stream in listener.incoming() {
            let stream = stream.unwrap();
            self.handle_stream(stream)?;
            return Ok(());
        }
        Ok(())
    }

    fn handle_stream<'b>(&mut self, mut stream: TcpStream) -> Result<()> {
        let mut buffer = [0; 1024];
        loop {
            let _n = match stream.read(&mut buffer[..]) {
                Ok(n) if n == 0 => return Ok(()),
                Ok(n) => {
                    let small_buffer: &[u8; 4] = <&[u8; 4]>::try_from(&buffer[..4])?;
                    let buffer_int = u32::from_be_bytes(*small_buffer);
                    let mut message_buffer = vec![0; buffer_int as usize];
                    buffer[4..n].clone_into(&mut message_buffer);
                    
                    let s = String::from_utf8(message_buffer)?;

                    let r = Message::try_parse_request(s, buffer_int as usize);
                    match r {
                        Err(e) => {
                            panic!("{}", e);
                        }
                        Ok(r) => {
                            match &r.payload.content {
                                MessageWithType::Request(request) => {
                                    //let selfBox = Box::new(self);
                                    let (func_type, content) = match self.parse_request(&request) {
                                        Ok(parsed) => (1, parsed.content),
                                        Err(e) => (2, e.to_string())
                                    };
                                    let serialized = "[".to_owned() + &func_type.to_string() + ", " + &r.payload.id.to_string() + ", " + &content + "]";
                                    let mut message_raw: Vec<u8> = vec![0; serialized.len() + 4];
                                    println!("Response message: {:?} {:?}", serialized.len() + 4, serialized);
                                    message_raw[..4].copy_from_slice(&(serialized.len() as u32).to_be_bytes()[..]);
                                    message_raw[4..].copy_from_slice(serialized.as_bytes());
                                    stream.write_all(&message_raw)?;
                                    match request.function {
                                        MosaikMethod::Stop => {
                                            return Ok(());
                                        }
                                        _ => ()
                                    } 
                                }
                                MessageWithType::Response(resp) => panic!("Received a response. That should never happen! Content: {:?}", resp.content),
                            }
                        }
                    }
                },
                Err(e) => {
                    eprintln!("failed to read from socket; err = {:?}", e);
                    return Err(e.into())
                }
            };
        }
    }

    fn parse_request<'b>(&'b mut self, request: &Request) -> Result<Response> {
        let content = match request.function {
            MosaikMethod::Init => {
                let sim_id = request.args[0].as_str().ok_or(anyhow!("Argument is not a string"))?;
                let default = 1f64;
                let time = request
                    .kwargs
                    .get("time_resolution")
                    .map_or(default, |x| x.as_f64().map_or(default, |y| y)); //.as_f64().take(); //["time_resolution"].as_f64().unwrap_or(1f64);
                serde_json::to_string(&self.simulator.initialize(sim_id.to_string(), time, &request.kwargs)?)?
            }
            MosaikMethod::Create => {
                let model_amount = request.args[0].as_u64().ok_or(anyhow!("Argument is not a number"))?;
                let model_name = request.args[1].as_str().ok_or(anyhow!("Argument is not a string"))?;
                let entities = self.simulator.create(model_amount, model_name, &request.kwargs)?;
                let m: Vec<_> = entities.iter().map(|entity| entity.to_model()).collect();
                serde_json::to_string(&m)?
            }
            MosaikMethod::SetupDone => {
                self.simulator.setup_done()?;
                serde_json::Value::Null.to_string()
            }
            MosaikMethod::Step => self.simulator
                .step(
                    request.args[0].as_f64().ok_or(anyhow!("Argument is not a string"))?,
                    request.args[1].as_object().ok_or(anyhow!("Request malformed! {:?}", request))?
                )?
                .to_string(),
            MosaikMethod::GetData => {
                serde_json::to_string(&self.simulator.get_data(request.args[0].as_object().ok_or(anyhow::anyhow!("Cannot parse output request"))?)?)?
            },
            MosaikMethod::Stop => {
                self.simulator.stop()?;
                serde_json::Value::Null.to_string()
            }
        };
        Ok(Response { content })
    }
}