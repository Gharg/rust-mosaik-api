use serde::Serialize;
use std::collections::HashMap;

#[derive(Serialize, Debug)]
pub enum SimulatorType {
    #[serde(rename = "time-based")]
    TimeBased,
    #[serde(rename = "event-based")]
    EventBased,
    #[serde(rename = "hybrid")]
    Hybrid,
}

#[derive(Serialize, Debug)]
pub struct Model<'a> {
    pub public: bool,
    pub params: &'a [&'a str],
    pub attrs: &'a [&'a str],
    pub any_inputs: bool,
}

#[derive(Serialize, Debug)]
pub struct Metadata<'a> {
    pub api_version: &'a str,
    pub simulator_type: SimulatorType,
    pub models: HashMap<&'a str, Model<'a>>,
    pub extra_methods: &'a [&'a str],
}
